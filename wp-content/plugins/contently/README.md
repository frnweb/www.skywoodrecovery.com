# Wordpress integration with Contently

### Versioning

The plugin follows _in spirit_ the conventions of [semantic versioning](http://semver.org/).

Given a version number MAJOR.MINOR.PATCH, increment the:

- MAJOR version when Contently's Stories API version changes
- MINOR version when there is a significant change (UI, etc) that attempts to be backwards compatible
- PATCH version when you make backwards-compatible bug fixes


### Deploying

1. Update the version number in ```index.php```
2. Run ```./bin/deploy```
3. Add the package as a [tagged release in Github](https://github.com/contently/integration_wordpress/releases) with the version number above


### Running Locally

The WordPress plugin can be run locally by going to the [`docker`](./docker) folder. From there, start MySQL and
Apache/Wordpress via:

```
docker-compose up
```

You can then go to [http://localhost:8000](http://localhost:8000) to view Wordpress. The first time you run this you
will need to setup WordPress. Follow the wizard instructions. After that, you will need to activate the plugins in the
environment, though they will be automatically installed.

In the docker container, the Contently plugin code is mapped as a volume to the code on your local machine, so edits will
show up live in Wordpress.


#### Connecting to MySQL

You can connect to the MySQL instance running in the container via the following information:

**Host:** `localhost`<br>
**Port:** `3306`<br>
**Username:** `wordpress`<br>
**Password:** `wordpress`<br>


#### Allowing Webhooks from Contently

You will want to use something like [ngrok](https://ngrok.com/) to allow the webhook be received from a deployed
Contently instance. 

Route ngrok traffic to local:

```
ngrok http 8000
```

Wordpress tries to redirect to the correct URL, so update it to your ngrok URL using SQL:

```
UPDATE wp_options 
SET option_value = "<ngrok_url>" 
WHERE option_name = "siteurl" or option_name = "home";
```

#### Debug Mode

Additional consoles are available in debug mode, which is enabled within the configuration for a publication: 

![steps to enable debug mode](./docs/images/debug_mode.png)

This option toggles the `debug_state` property `1`/`0` in the `cl_options` value of the `wp_options` table:

```
SELECT *
FROM wp_options
WHERE option_name = "cl_options";
```

You can also set debug mode using this SQL statement, but make sure there aren't any other options stored in the 
serialized field first, as this will overwrite those:

```
UPDATE wp_options
SET option_value = "a:1:{s:11:\"debug_state\";b:1;}"
WHERE option_name = "cl_options";
```


#### Stopping Local Development Server

To stop `ctl-c` if docker compose is still running interactively, or

```
docker-compose down
```