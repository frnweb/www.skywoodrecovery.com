<?php

namespace Contently\Log\Drivers;

class Filesystem implements DriverInterface {

	private $DRIVER = 'local file';

	private $config = array(
		'state'     => false,
		'file'      => ""
	);

	private $handler = null;

	public function __construct( Array $config = array() ) {
		if ( $config ) {
			$this->config = array_merge( $this->config, $config );
		}

		if ( ! $this->config['state'] ) {
			return false;
		}

		if ( empty( $this->config['file'] ) ) {
			$this->config['file'] = dirname( __FILE__ ) . '/../.log/contently.log';
		}

		$file_name = $this->config['file'];
		if ( $file_name && ! file_exists( $file_name ) ) {
			@mkdir( dirname( $file_name ) );
		}
		$this->handler = @fopen( $file_name, 'a+' );
	}

	public function getDriverDescription() {
		return $this->DRIVER;
	}

	public function hasHandler() {
		return gettype( $this->handler ) === 'resource';
	}

	public function getSize() {
        $file_name = $this->config['file'];
        if ( $file_name && file_exists( $file_name ) ) {
            return filesize( $file_name );
        }

        return 0;
    }

    /**
	 * @param string $data
	 *
	 * @return bool
	 */
	public function log( $data = '' ) {
		if ( $this->config['state'] && $this->handler ) {

			return (bool) @fwrite( $this->handler, $data . PHP_EOL );
		}

		return false;
	}

	public function getData() {
		$data = @file_get_contents( $this->config['file'] );
		$data = explode( PHP_EOL, $data );

		$result = array();
		foreach ( $data as $row ) {
            if (! empty( $row ) ) {
                $result[] = json_decode( $row, JSON_OBJECT_AS_ARRAY );
            }
		}

		return $result;
	}

	public function clear( $timestamp = false ) {
        if (! $this->handler ) {
            return false;
        }

        if ( false === $timestamp ) {
            @ftruncate( $this->handler, 0 );
            return true;
        }

        $output = array();
        @rewind( $this->handler );

        while( ! @feof( $this->handler ) ) {
            $data = json_decode( @fgets( $this->handler ), true );
            if ( is_array( $data ) && isset( $data['timestamp'] ) ) {
                if ( (int) $data['timestamp'] >= (int) $timestamp ) {
                    array_push( $output, json_encode( $data ) );
                }
            }
        }

        @ftruncate( $this->handler, 0 );
        array_map( array( $this, 'log' ), $output );

        return true;
    }

	public function remove() {
        if ( $this->handler ) {
            @fclose( $this->handler );
            $this->handler = null;
        }
		$file_name = $this->config['file'];
		@unlink( $file_name );

		return @rmdir( dirname( $file_name ) );
	}

}