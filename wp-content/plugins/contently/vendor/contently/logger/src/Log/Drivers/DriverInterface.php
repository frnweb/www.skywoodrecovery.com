<?php
namespace Contently\Log\Drivers;

interface DriverInterface {

	public function getDriverDescription();

	public function hasHandler();

	public function getData();

    public function getSize();

	public function log( $data );

    public function clear( $timestamp );

    public function remove();

}