<?php
/*
Template Name: Contact Page Template
*/
?>
<?php get_header(); ?>
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php 
					$src = get_page_banner();
				?>
				<header>
					<div class="page-header interior-top-banner blog-stretchy-wrapper" style="background-image: url('<?php echo $src; ?>');">
						<div>
							<div class="row interior-top-text-box">
								<div class="small-12 columns">
									<div style="display: table; width: 100%;">
										<div style="display: table-cell; vertical-align: middle;">
											<h1 class="page-title" style="color: #fff;" itemprop="headline"><?php the_title(); ?></h1>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header> <!-- end article header -->
				
					<div id="main" class="" role="main">

						
						
						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">				
							
							
						
							<section class="post_content clearfix" itemprop="articleBody">
										
											<div class="row tb-pad-40">
												<div id="main" class="large-6 columns clearfix " role="main">
													<?php the_post_thumbnail( 'full' ); ?>
													<?php /*echo '<h1>Content part '.$content_count.'</h1>';*/ ?>
													<?php the_content(); ?>

													<?php /*
													//Dax commented this out 9/26/17 and used latest FRN plugin shortcode since it now controls when chat goes offline.
													<a onclick="window.open('http://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160','Ticket','left=' + (screen.width - 550-32) / 2 + ',top=50,scrollbars=yes,menubar=no,height=550,width=450,resizable=yes,toolbar=no,location=no,status=no');return false;" href="http://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160" rel="external" target="_blank" class="button">Email Us</a>
													
													 if(is_chat_live()): 
													<a href="#" id="snChat" class="chat-launch button">Chat With Us</a>
													 endif; 
													*/
													?>
													
													
													<?php wp_link_pages(); ?>
													<?php 
														// only show edit button if user has permission to edit posts
														if( $user_level > 0 ) { 
													?>
														<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post"><i class="icon-pencil icon-white"></i> <?php _e("Edit post","wpbootstrap"); ?></a>
													<?php } ?>
												</div> <!-- end #main -->
												<div class="large-6 columns">
													<?php 
														$url = get_field('video_embed');
														if($url){
															$code = get_youtube_code($url);
															?>
																<div class="flex-video widescreen">
																  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $code ?>"></iframe>
																</div>
															<?php 
														}
													?>
													<?php
														$map_code = get_field('google_maps_place_phase');
														if($map_code){
															?>
														<!-- 4:3 aspect ratio -->
														<div class="flex-video widescreen">
														  <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1472.8036307945881!2d-85.3006646908812!3d42.41480739992067!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88178b10a0e28d33%3A0xec2d321769691ded!2sSkywood+Recovery!5e0!3m2!1sen!2sus!4v1458583600962"></iframe>
<?php //original: https://www.google.com/maps/embed/v1/place?key=AIzaSyBYLnTIgXEnDwXJZQHNyJZkNxl8CxSYAoI&amp;q= ?><?php //echo $map_code ?> 
														</div>

															<?php
														}
													
													?>
												</div>
											</div>
						
							</section> <!-- end article section -->

							
							<footer>
				
								
								
							</footer> <!-- end article footer -->
						
						</article> <!-- end article -->
						
						<?php echo get_resources_block(); ?>
						
						
				
					</div> <!-- end #main -->
		 
					
		 
			
			<?php endwhile; ?>		
					
			<?php endif; ?>

<?php get_footer(); ?>