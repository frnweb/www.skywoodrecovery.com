Contact 7 Skywood Recovery Assessment confirmation

To: [your-name] <[your-email]>
From: Skywood Recovery <no-reply@skywoodrecovery.com>
Subject: Assessment Complete

<font face="Arial, Georgia, Serif" size="2">
Thank you for contacting us and we are proud to help. Someone will be in contact with you shortly to schedule a time to speak with you. We care about your situation and want to do all we can to help.

As confirmation, the information submitted to our team is below. Please do not reply to this email since the account is not monitored. You can contact us anytime via (855) 317-8377 or by using one of our other <a href="http://skywoodrecovery.com/contact/">contact methods</a>. In case you are asked, this message is stored in our LiveHelpNow encrypted and private system using the email address you provided in accordance with HIPAA and federal laws. Your privacy is one of our highest concerns and we will do everything necessary to protect it. You will not receive any other forms of communication from us other than what's related to this assessment without your prior consent.

Sincerely,
Skywood Recovery Team


<hr>

<b>From:</b> [your-name] <[your-email]>
<b>Phone:</b> [your-phone]

<b>Preferred Contact Method:</b> [contact_choice]

<b>Data:</b>
[assess_data]


&nbsp;
&nbsp;
&nbsp;
<hr>
<small>This assessment was performed on Skywood Recovery's homepage (http://skywoodrecovery.com). All information is private and confidential.</small></font>