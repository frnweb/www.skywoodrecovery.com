var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    sourcemaps  = require('gulp-sourcemaps');
    rename      = require('gulp-rename'),
    cssmin      = require('gulp-clean-css'),
    cache       = require('gulp-cached'),
    prefix      = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload,
    size        = require('gulp-size'),
    plumber     = require('gulp-plumber');


gulp.task('scss', function() {
  return gulp.src('assets/scss/app.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(prefix())
    .pipe(rename('frank.css'))
    .pipe(gulp.dest('assets/dist'))
    .pipe(reload({stream:true}))
    .pipe(cssmin())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('assets/dist'))
});

gulp.task('browser-sync', function() {
    browserSync({
        proxy: "skywoodrecoverycom.lndo.site"
    });
});


gulp.task('watch', function() {
  gulp.watch('assets/scss/**/*.scss', ['scss']);
  gulp.watch('page-frank.php').on('change', browserSync.reload);
});

gulp.task('default', ['browser-sync', 'scss', 'watch']);
