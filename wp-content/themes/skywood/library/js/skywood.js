var effectsCutoff = 1024

function checkMegaMenu() {
	$('#mega-menu').css('overflow-y', 'auto');
	$('#mega-menu').height('');
	var winHeight = $(window).height();
	var megaNavDesiredHeight = winHeight*1;
	var megaNavDefaultHeight = $('#mega-menu').outerHeight()+$('#large-nav').outerHeight();
	if(megaNavDesiredHeight < megaNavDefaultHeight) {
		$('#mega-menu').height(megaNavDesiredHeight);
		$('#mega-menu').css('overflow-y', 'scroll');
	}
}
function closeMegaMenu() {
	$('#site-header').removeClass('nav-opened');
	$('#mega-btn').removeClass('nav-opened');
	$('#mega-menu').slideUp();
}
function closeMegaMenuFast() {
	$('#site-header').removeClass('nav-opened');
	$('#mega-btn').removeClass('nav-opened');
	$('#mega-menu').hide();
}

var topSearchOpened = false;
var topSearchOpening = false;
function openTopSearch() {
	closeMegaMenu();
	$('#main-nav').addClass('search-opened');
	$('#top-search').slideDown( "normal", function() {
		$('#top-search #search').focus();
	});
	topSearchOpened = true;
	topSearchOpening = true;
	setTimeout(function(){
		topSearchOpening = false;
	},300);
}
function closeTopSearch() {
	$('#main-nav').removeClass('search-opened');
	$('#top-search').slideUp();
	topSearchOpened = false;
}

var previousScroll = 0;
function resourceSideNav() {
	var ua = window.navigator.userAgent;
   var msie = ua.indexOf("MSIE ");
	var link_box_height = $('#sidebar-link-box').outerHeight();
	$('#sidebar1').css('minHeight', link_box_height);
	if($('#sidebar-link-box').length && msie <= 0) {
		if($(window).width() > effectsCutoff) {
			var topNavHeight = $('#main-nav').outerHeight();
			var sideBar1Offset = $('#sidebar1').offset().top;
			var calculated_window_height = $(window).outerHeight()-topNavHeight;
			
			
			var fullSideBar = $('#sidebar1').outerHeight();
			if(previousScroll <= $(window).scrollTop()) {
				if(link_box_height >= calculated_window_height) {
					var link_box_offset = link_box_height-calculated_window_height
					var newBottomVal = Math.round((fullSideBar)-($(window).scrollTop()-(sideBar1Offset-topNavHeight))-(link_box_height)+link_box_offset);
					if(($(window).scrollTop()+$(window).outerHeight()) >= ($('#sidebar-link-box').outerHeight()+$('#sidebar-link-box').offset().top)) {
						if((newBottomVal > 0 && newBottomVal <= (fullSideBar - link_box_height))) {
							$('#sidebar-link-box').css('top', 'auto');
							$('#sidebar-link-box').css('bottom', newBottomVal);
						} else {
							$('#sidebar-link-box').css('top', 'auto');
							$('#sidebar-link-box').css('bottom', 0);
						}
					}
				} else {
					var newBottomVal = Math.round((fullSideBar)-($(window).scrollTop()-sideBar1Offset)-(link_box_height+topNavHeight));
					if(($(window).scrollTop()+topNavHeight) >= $('#sidebar-link-box').offset().top) {
						if((newBottomVal > 0 && newBottomVal <= (fullSideBar - link_box_height))) {
							$('#sidebar-link-box').css('top', 'auto');
							$('#sidebar-link-box').css('bottom', newBottomVal);
						} else {
							$('#sidebar-link-box').css('top', 'auto');
							$('#sidebar-link-box').css('bottom', 0);
						}
					}
				}
			} else {
				var newTopVal = Math.round(($(window).scrollTop()-sideBar1Offset)+(topNavHeight));
				if(($(window).scrollTop()+topNavHeight) <= $('#sidebar-link-box').offset().top) {
					if((newTopVal > 0 && newTopVal <= (fullSideBar - link_box_height))) {
						$('#sidebar-link-box').css('bottom', 'auto');
						$('#sidebar-link-box').css('top', newTopVal);
					} else {
						$('#sidebar-link-box').css('bottom', 'auto');
						$('#sidebar-link-box').css('top', 0);
					}
				}
			}
			previousScroll = $(window).scrollTop();
		} else {
			$('#sidebar-link-box').css('bottom', '');
			$('#sidebar-link-box').css('top', '');
		}
	}
}

function parallaxBG(element, speed, direction){
	if($(window).width() > effectsCutoff) {
		if(element.length) {
			if($(window).height() <= element.offset().top) {
				var pscrolled = ($(window).scrollTop())-((element.offset().top));
			} else {
				//var pscrolled = ($(window).scrollTop())-((element.offset().top)+((element.outerHeight()/2)/speed));
				var pscrolled = ($(window).scrollTop())-((element.offset().top));
			}
			if(pscrolled < 0) {
				//scrolled = 0;
			}
			//console.log(pscrolled);
			if(($(window).scrollTop()+($(window).height())) >= element.offset().top) {
				element.css({'background-position': 'center top ' + Math.round(direction*(pscrolled*speed))+'px'}); //center bottom 0px;
			}
		}
	} else {
		element.css({'background-position': ''});
	}
}
function parallaxMargin(element, speed, direction){
	if($(window).width() > effectsCutoff) {
		if(element.length) {
			if($(window).height() <= element.offset().top) {
				var scrolled = ($(window).scrollTop()+$(window).height())-element.offset().top;
			} else {
				var scrolled = $(window).scrollTop();
			}
			if(scrolled < 0) {
				//scrolled = 0;
			}
			if(($(window).scrollTop()+($(window).height())) >= element.offset().top) {
				element.css({'margin-top': Math.round(direction*(scrolled*speed))+'px'}); //center bottom 0px;
			}
		}
	} else {
		element.css({'margin-top': ''});
	}
}
function parallaxCallout() {
	
	if($('#parallax-callout-bg').length) {
		if($(window).width() > effectsCutoff) {
			var scrollSpeed = 0.40;
			var paraCalloutOffset = (-1*$('#parallax-bg-callout').offset().top*scrollSpeed)-$('#large-nav').outerHeight();
			
			$('#parallax-callout-bg').css({'background-position': 'center top '+paraCalloutOffset+'px'});
			if($('#parallax-bg-callout').outerHeight() > $(window).height()) {
				var paraCalloutCurPos = paraCalloutOffset+($(window).scrollTop()*scrollSpeed)-($('#parallax-bg-callout').outerHeight()/4);
			} else {
				var paraCalloutCurPos = paraCalloutOffset+($(window).scrollTop()*scrollSpeed)-($(window).height()/4);
			}
			$('#parallax-callout-bg').css({'background-position': 'center top '+paraCalloutCurPos+'px'});
		} else {
			
		}
		if($('#parallax-bg-callout').outerHeight() > $(window).height()) {
			$('#parallax-callout-bg').height(Math.ceil(($('#parallax-bg-callout').outerHeight()*(1+scrollSpeed))));
		} else {
			$('#parallax-callout-bg').height(Math.ceil(($(window).height()*(1+scrollSpeed))));
		}
	}
}
function navLogoExpander() {
	var logoMarginRight = 32;
	if($(window).width() <= effectsCutoff) {
		logoMarginRight = 8;
	}
	if($('#navStopper .sticky').hasClass('is-stuck')) {
		if($('#nav-logo').hasClass('hide-logo')) {
			reStickSideNavAnimate();
			$('#nav-logo').animate({'width':'136px', 'opacity':'1', 'margin-right': logoMarginRight+'px'}, {'duration':600});
			$('.phone-menu > li > div').animate({'opacity':'1'}, {'duration':600});
			$('#nav-logo').removeClass('hide-logo');
			$('#nav-logo').addClass('show-logo');
		}
	} else {
		if($('#nav-logo').hasClass('show-logo')) {
			reStickSideNavAnimate();
			$('#nav-logo').animate({'width':'0px', 'opacity':'0', 'margin-right': '0px'}, {'duration':600});
			$('.phone-menu > li > div').animate({'opacity':'0'}, {'duration':400});
			$('#nav-logo').removeClass('show-logo');
			$('#nav-logo').addClass('hide-logo');
		}
	}
}


// Side nav
function reStickSideNav() {
	if($('#main-nav').hasClass('wpadmin-active')) {
		var addSNSpace = 52;
	} else {
		var addSNSpace = 20;
	}
	var hnHeight = $('#global-side-nav').height();
	var hnTopOffset = ($('#main-nav').height() + $('#site-header').outerHeight() + addSNSpace);
	$('#global-side-nav').css('top', hnTopOffset);
}
function reStickSideNavAnimate() {
	/*var hnHeight = $('#global-side-nav').height();
	if($('#navStopper .sticky').hasClass('is-stuck')) {
		var hnTopOffset = ($('#main-nav').height() + ($('#main-nav').offset().top - $(window).scrollTop()) + 20);
	} else {
		if($('#main-nav').hasClass('wpadmin-active')) {
			var addSNSpace = 52;
		} else {
			var addSNSpace = 20;
		}
		var hnTopOffset = ($('#main-nav').outerHeight() + $('#site-header').outerHeight() + addSNSpace);
	}
	$('#global-side-nav').animate({top: hnTopOffset}, 500);*/
}
function openSideNav() {
	if($(window).width() > effectsCutoff) {
		if(!$('#global-side-nav.collapsed').hasClass('hold')) {
			$('#global-side-nav.collapsed:not(.hold)').animate({ width: "185px"} , 800);
			$('#global-side-nav').removeClass('collapsed');
			$('#global-side-nav').addClass('expanded');
		}
	}
}
function checkSideNavState() {
	if($(window).width() > effectsCutoff) {
		if($('#global-side-nav').hasClass('expanded') && $(window).scrollTop() > 10) {
			$('#global-side-nav.expanded').animate({ width: "42px"} , 800);
			$('#global-side-nav').removeClass('expanded');
			$('#global-side-nav').addClass('collapsed');
			$('#global-side-nav').addClass('hold');
			setTimeout(function(){
				$('#global-side-nav').removeClass('hold');
			}, 1000);
		} else if($('#global-side-nav').hasClass('collapsed') && $(window).scrollTop() <= 10) {
			openSideNav();
		}
	}
}
// Resource Nav Mobile
sideNavMobileOpened = false;
function openSideNavMobile() {
	$('#sidebar-menu-items').slideDown();
	sideNavMobileOpened = true;
}
function closeSideNavMobile() {
	$('#sidebar-menu-items').slideUp();
	sideNavMobileOpened = false;
}

jQuery(document).ready(function( $ ) {
	var ua = window.navigator.userAgent;
   var msie = ua.indexOf("MSIE ");
   if(navigator.userAgent.match(/Trident\/7\./)) {
		 $('body').on("mousewheel", function () {
			  event.preventDefault();
			  var wd = event.wheelDelta;
			  var csp = window.pageYOffset;
			  window.scrollTo(0, csp - wd);
		 });
	} else if(ua.indexOf('Edge/') > 0) {
		$('body').on("mousewheel", function () {
			  event.preventDefault();
			  var wd = event.wheelDelta;
			  var csp = window.pageYOffset;
			  window.scrollTo(0, csp - wd);
		 });
		/*console.log('Edge');*/
	} else if(msie > 0) {
		
	}
	
	//used in the resource menu
	jQuery('#sidebar-menu-icon a').click(function(e){
		if(jQuery(this).hasClass('minus')){
			jQuery('#word-change').html('More');
		} else{
			jQuery('#word-change').html('Less');
		}
		jQuery(this).toggleClass('minus');
	});;
	
	$('.temp-link-block > a').attr('href', '/');
	$('.temp-link-block > a').css( 'cursor', 'default' );
	$('.temp-link-block > a').click(function(evt){
		evt.preventDefault();
	});
	
	if (document.location.href.indexOf('/category') > -1 || document.location.href.indexOf('/resource') > -1) {
		$('#menu-main-menu li.current_page_parent').addClass('active');
	}
	$('#sidebar-menu-icon a').click(function(evt) {
		evt.preventDefault();
		if(sideNavMobileOpened) {
			closeSideNavMobile();
		} else {
			openSideNavMobile();
		}
	});
	
	$('.open-top-search').click(function(evt){
		evt.preventDefault();
		if($('#main-nav').hasClass('search-opened')) {
			closeTopSearch();
		} else {
			openTopSearch();
		}
	});
	$('.chat-launch').click(function(evt){
		closeMegaMenu();
	});
   $('body').click(function(e) {
		var target = $(e.target);
		if(!topSearchOpening && topSearchOpened && !target.is('#top-search *')) {
			closeTopSearch();
		} else if(!target.is('#mega-btn') && !target.is('#mega-menu') && !target.is('#mega-menu *') && !target.is('#large-nav') && !target.is('#large-nav *') && $('#site-header').hasClass('nav-opened')) {
			closeMegaMenu();
		}
	});
	$('#mega-btn').click(function(evt){
		evt.preventDefault();
		closeTopSearch();
		if($('#site-header').hasClass('nav-opened')) {
			$('#site-header').removeClass('nav-opened');
			$('#mega-btn').removeClass('nav-opened');
			$('#mega-menu').slideUp();
		} else {
			$('#site-header').addClass('nav-opened');
			$('#mega-btn').addClass('nav-opened');
			$('body').addClass('overflow-hidden');
			$('#mega-menu').slideDown();
			setTimeout(function(){
				checkMegaMenu();
			}, 500);
		}
	});
	
	$('ul.mm-nav > div.columns').wrapAll('<div class="row"></div>');
	if(document.getElementById("wpadminbar")) { 
		$('#main-nav').addClass('wpadmin-active');
	}
	$(window).resize(function(){
		if($(window).width() > 500) {
			closeMegaMenuFast();
		}
		if($(window).width() >= 1024) {
			if(!sideNavMobileOpened) {
				openSideNavMobile();
			}
		} else {
			if(sideNavMobileOpened) {
				closeSideNavMobile();
			}
		}
	});
	
	
	$('#staff-page .more-link').click(function(evt){
		evt.preventDefault();
		$(this).closest('.columns').find('.staff-excerpt-content').hide();
		$(this).closest('.columns').find('.staff-full-content').slideDown();
	});
	
	/*$('.more-link').before('<br><br>');
	$('.more-link').addClass('button').addClass('large').addClass('secondary');*/
	$('#sidebar1 ul li a').click(function(evt){
		setTimeout(function(){
			new Foundation.Equalizer($("#sidebar1").closest('.row')).applyHeight();
		}, 415);
		if(!$(this).hasClass('open-item') && !$(this).parents().hasClass('open-item')) {
			evt.preventDefault();
			$('#sidebar1 ul li ul').slideUp();
			$('#sidebar1 ul li').removeClass('open-item');
			$('#sidebar1 ul li a').removeClass('open-item');
			$(this).addClass('open-item');
			$(this).parent().addClass('open-item');
			$(this).siblings('ul').slideDown('normal', function(){
				var link_box_height = $('#sidebar-link-box').outerHeight();
				$('#sidebar1').css('minHeight', link_box_height);
			});
			new Foundation.Equalizer($("#sidebar1").closest('.row')).applyHeight();
		} else {
			return true;
		}
	});
	
	setTimeout(function(){
		reStickSideNav();
		$('#global-side-nav').animate({ right: '0px'} , 600);
	}, 300);
	$('#global-side-nav a').hover(function(){
		openSideNav();
	});
	navLogoExpander();
	parallaxBG($('#call-us'), 0.25, -1);
	parallaxBG($('.interior-top-banner'), 0.25, 1);
	parallaxMargin($('.interior-top-banner .page-title'), 0.6, 1);
	parallaxMargin($('.interior-top-banner .single-title'), 0.6, 1);
	parallaxCallout();
	resourceSideNav();
	$(window).scroll(function(){
		parallaxBG($('#call-us'), 0.25, -1);
		parallaxBG($('.interior-top-banner'), 0.25, 1);
		parallaxMargin($('.interior-top-banner .page-title'), 0.6, 1);
		parallaxMargin($('.interior-top-banner .single-title'), 0.6, 1);
		parallaxCallout();
		navLogoExpander();
		checkSideNavState();
		resourceSideNav();
	});
	//remove default events from the sharethis plugin
	$('body').off('mouseover', '.st_sharethis');
	$('.st_sharethis').off('mouseover');
	setTimeout(function(){
		$('body').off('mouseover', '.st_sharethis');
		$('.st_sharethis').off('mouseover');		
	}, 500
	
	);
});
$(window).load(function(){
	//$(window).trigger('resize');
});